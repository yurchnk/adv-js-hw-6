const button = document.querySelector('.btn');

async function getIpAddress() {
    const url = "https://api.ipify.org/?format=json";
  
    try {
      const response = await fetch(url);
      if (response.ok) {
        const data = await response.json();
        const ipAddress = data.ip;
        console.log("IP:", ipAddress);
        return await ipAddress;
      } else {
        console.log("Error Ip Adress.");
      }
    } catch (error) {
      console.error("Error!", error);
    }
  }

async function getInfoFromIp(ip) {
    const url = `http://ip-api.com/json/${ip}?fields=status,continent,country,regionName,city,district,query`;

    try {
        const response = await fetch(url);
        if (response.ok) {
            const data = await response.json();
            if(data.status == 'success'){
                console.log("Info:", data);
                return await [data.continent, data.country,data.regionName, data.city];
            }else{
                console.log('Error data from site');
            }
            } else {
        console.log("Error Ip Adress.");
        }
    } catch (error) {
        console.error("Error!", error);
    }
}

button.addEventListener('click', async function() {
    let ip = await getIpAddress();
    let [continent, country, regionName, city] = await getInfoFromIp(ip);
    console.log(continent, country, regionName, city);
    let div_info = document.createElement('div');
    div_info.innerText  = `Continent = ${continent}\nCountry = ${country}\nRegion = ${regionName}\nCity = ${city}`;
    document.querySelector('.btn').after(div_info);
});